\changetocdepth {4}
\select@language {brazil}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Introdu\IeC {\c c}\IeC {\~a}o}{13}{chapter*.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {1}\MakeTextUppercase {Abordagem de Engenharia de Requisitos}}{15}{chapter.1}
\contentsline {section}{\numberline {1.1}Abordagem Tradicional}{15}{section.1.1}
\contentsline {section}{\numberline {1.2}Abordagem \IeC {\'A}gil}{17}{section.1.2}
\contentsline {section}{\numberline {1.3}Modelos de Maturidade}{18}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}CMMI}{18}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}MPS.BR}{20}{subsection.1.3.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {2}\MakeTextUppercase {Contexto}}{23}{chapter.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {3}\MakeTextUppercase {Justificativa da Abordagem}}{25}{chapter.3}
\contentsline {section}{\numberline {3.1}Escolha do Modelo de Maturidade}{25}{section.3.1}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {4}\MakeTextUppercase {Processo de Engenharia de Requisitos}}{27}{chapter.4}
\contentsline {section}{\numberline {4.1}Analisar Problema}{28}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Processo}{28}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Pap\IeC {\'e}is}{29}{subsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.2.1}Analista de Sistema}{29}{subsubsection.4.1.2.1}
\contentsline {subsection}{\numberline {4.1.3}Artefatos}{29}{subsection.4.1.3}
\contentsline {subsubsection}{\numberline {4.1.3.1}Regras de Neg\IeC {\'o}cio}{29}{subsubsection.4.1.3.1}
\contentsline {subsubsection}{\numberline {4.1.3.2}Gloss\IeC {\'a}rio}{29}{subsubsection.4.1.3.2}
\contentsline {subsubsection}{\numberline {4.1.3.3}Documento de Vis\IeC {\~a}o (inicial)}{29}{subsubsection.4.1.3.3}
\contentsline {subsubsection}{\numberline {4.1.3.4}Modelo de Casos de Uso (Somente Atores)}{29}{subsubsection.4.1.3.4}
\contentsline {subsection}{\numberline {4.1.4}Atividades}{29}{subsection.4.1.4}
\contentsline {subsubsection}{\numberline {4.1.4.1}Entender contexto da empresa}{29}{subsubsection.4.1.4.1}
\contentsline {subsubsection}{\numberline {4.1.4.2}Capturar Vocabul\IeC {\'a}rio Comum}{30}{subsubsection.4.1.4.2}
\contentsline {subsubsection}{\numberline {4.1.4.3}Desenvolver Vis\IeC {\~a}o}{30}{subsubsection.4.1.4.3}
\contentsline {subsubsection}{\numberline {4.1.4.4}Localizar Atores}{30}{subsubsection.4.1.4.4}
\contentsline {subsubsection}{\numberline {4.1.4.5}Descrever intera\IeC {\c c}\IeC {\~a}o entre atores e problema}{30}{subsubsection.4.1.4.5}
\contentsline {section}{\numberline {4.2}Entendendo Necessidades dos Envolvidos}{31}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Processo}{31}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Pap\IeC {\'e}is}{32}{subsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.2.1}Analista de Sistema}{32}{subsubsection.4.2.2.1}
\contentsline {subsection}{\numberline {4.2.3}Artefatos}{32}{subsection.4.2.3}
\contentsline {subsubsection}{\numberline {4.2.3.1}Solicita\IeC {\c c}\IeC {\~a}o dos principais \textit {stakeholders}}{32}{subsubsection.4.2.3.1}
\contentsline {subsubsection}{\numberline {4.2.3.2}Gloss\IeC {\'a}rio}{32}{subsubsection.4.2.3.2}
\contentsline {subsubsection}{\numberline {4.2.3.3}Documento de Vis\IeC {\~a}o (refinado)}{32}{subsubsection.4.2.3.3}
\contentsline {subsubsection}{\numberline {4.2.3.4}Especifica\IeC {\c c}\IeC {\~a}o Suplementar}{32}{subsubsection.4.2.3.4}
\contentsline {subsubsection}{\numberline {4.2.3.5}Modelo de Casos de Uso}{32}{subsubsection.4.2.3.5}
\contentsline {subsection}{\numberline {4.2.4}Atividades}{32}{subsection.4.2.4}
\contentsline {subsubsection}{\numberline {4.2.4.1}Identificar solicita\IeC {\c c}\IeC {\~a}o dos principais \textit {stakeholders}}{32}{subsubsection.4.2.4.1}
\contentsline {subsubsection}{\numberline {4.2.4.2}Capturar vocabul\IeC {\'a}rio comum}{33}{subsubsection.4.2.4.2}
\contentsline {subsubsection}{\numberline {4.2.4.3}Desenvolver Vis\IeC {\~a}o}{33}{subsubsection.4.2.4.3}
\contentsline {subsubsection}{\numberline {4.2.4.4}Desenvolver Especifica\IeC {\c c}\IeC {\~a}o Suplementar}{33}{subsubsection.4.2.4.4}
\contentsline {subsubsection}{\numberline {4.2.4.5}Estruturar o Modelo de Casos de Uso}{33}{subsubsection.4.2.4.5}
\contentsline {section}{\numberline {4.3}Validar Modelo de Casos de Uso}{34}{section.4.3}
\contentsline {section}{\numberline {4.4}Especificar Casos de Uso}{34}{section.4.4}
\contentsline {section}{\numberline {4.5}Priorizar Casos de Uso}{34}{section.4.5}
\contentsline {section}{\numberline {4.6}Desenvolver Prot\IeC {\'o}tipo de Interface}{34}{section.4.6}
\contentsline {section}{\numberline {4.7}Validar Prot\IeC {\'o}tipo}{35}{section.4.7}
\contentsline {section}{\numberline {4.8}Implementar atividades}{35}{section.4.8}
\contentsline {section}{\numberline {4.9}Ger\IeC {\^e}ncia de mudan\IeC {\c c}as}{35}{section.4.9}
\contentsline {subsection}{\numberline {4.9.1}Processo}{35}{subsection.4.9.1}
\contentsline {subsection}{\numberline {4.9.2}Pap\IeC {\'e}is}{37}{subsection.4.9.2}
\contentsline {subsubsection}{\numberline {4.9.2.1}Revisor t\IeC {\'e}cnico}{37}{subsubsection.4.9.2.1}
\contentsline {subsection}{\numberline {4.9.3}Artefatos}{37}{subsection.4.9.3}
\contentsline {subsubsection}{\numberline {4.9.3.1}Solicita\IeC {\c c}\IeC {\~a}o de mudan\IeC {\c c}a}{37}{subsubsection.4.9.3.1}
\contentsline {subsubsection}{\numberline {4.9.3.2}Relat\IeC {\'o}rio de Impacto das mudan\IeC {\c c}as}{37}{subsubsection.4.9.3.2}
\contentsline {subsubsection}{\numberline {4.9.3.3}Relat\IeC {\'o}rio de Avalia\IeC {\c c}\IeC {\~a}o das mudan\IeC {\c c}as}{37}{subsubsection.4.9.3.3}
\contentsline {subsection}{\numberline {4.9.4}Atividades}{37}{subsection.4.9.4}
\contentsline {subsubsection}{\numberline {4.9.4.1}Analisar impacto das mudan\IeC {\c c}as}{37}{subsubsection.4.9.4.1}
\contentsline {subsubsection}{\numberline {4.9.4.2}Analisar viabilidade das mudan\IeC {\c c}as}{37}{subsubsection.4.9.4.2}
\contentsline {subsubsection}{\numberline {4.9.4.3}Implementar mudan\IeC {\c c}as}{38}{subsubsection.4.9.4.3}
\contentsline {subsubsection}{\numberline {4.9.4.4}Melhorar/Atualizar artefatos}{38}{subsubsection.4.9.4.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {5}\MakeTextUppercase {Processo X Modelo de Maturidade}}{39}{chapter.5}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {6}\MakeTextUppercase {Elicita\IeC {\c c}\IeC {\~a}o de Requisitos}}{41}{chapter.6}
\contentsline {section}{\numberline {6.1}T\IeC {\'e}cnicas de elicita\IeC {\c c}\IeC {\~a}o de requisitos}{41}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Prototipagem}{41}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Entrevista}{41}{subsection.6.1.2}
\contentsline {subsection}{\numberline {6.1.3}\textit {Brainstorming}}{41}{subsection.6.1.3}
\contentsline {subsection}{\numberline {6.1.4}\textit {Workshop}}{42}{subsection.6.1.4}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {7}\MakeTextUppercase {Rastreabilidade}}{43}{chapter.7}
\contentsline {section}{\numberline {7.1}Estrat\IeC {\'e}gia de rastreabilidade}{43}{section.7.1}
\contentsline {section}{\numberline {7.2}Atributos de requisitos}{46}{section.7.2}
\contentsline {subsection}{\numberline {7.2.1}Estabilidade}{46}{subsection.7.2.1}
\contentsline {subsection}{\numberline {7.2.2}Prioridade}{46}{subsection.7.2.2}
\contentsline {subsection}{\numberline {7.2.3}Dificuldade}{48}{subsection.7.2.3}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {8}\MakeTextUppercase {Planejamento do Projeto}}{49}{chapter.8}
\contentsline {section}{\numberline {8.1}Fases}{49}{section.8.1}
\contentsline {subsection}{\numberline {8.1.1}Planejamento}{49}{subsection.8.1.1}
\contentsline {subsection}{\numberline {8.1.2}Elicita\IeC {\c c}\IeC {\~a}o}{49}{subsection.8.1.2}
\contentsline {subsection}{\numberline {8.1.3}Documenta\IeC {\c c}\IeC {\~a}o}{50}{subsection.8.1.3}
\contentsline {subsection}{\numberline {8.1.4}Valida\IeC {\c c}\IeC {\~a}o}{50}{subsection.8.1.4}
\contentsline {subsection}{\numberline {8.1.5}Gerenciamento dos requisitos}{50}{subsection.8.1.5}
\contentsline {section}{\numberline {8.2}Cronograma}{51}{section.8.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {9}\MakeTextUppercase {Ferramenta de Gest\IeC {\~a}o de Requisitos}}{53}{chapter.9}
\contentsline {section}{\numberline {9.1}Escolha da ferramenta}{53}{section.9.1}
\contentsline {subsection}{\numberline {9.1.1}\textit {Rational RequisitePro}}{53}{subsection.9.1.1}
\contentsline {subsection}{\numberline {9.1.2}\textit {RequirementOne}}{54}{subsection.9.1.2}
\contentsline {subsection}{\numberline {9.1.3}\textit {Visual Use Case}}{55}{subsection.9.1.3}
\contentsline {section}{\numberline {9.2}Crit\IeC {\'e}rio de escolha da ferramenta}{56}{section.9.2}
\setlength {\cftchapterindent }{0em} \setlength {\cftchapternumwidth }{\cftlastnumwidth }
\contentsline {chapter}{\chapternumberline {10}\MakeTextUppercase {Considera\IeC {\c c}\IeC {\~o}es Finais}}{57}{chapter.10}
\vspace {\cftbeforechapterskip }
\setlength {\cftchapterindent }{\cftlastnumwidth } \setlength {\cftchapternumwidth }{2em}
\contentsline {chapter}{Refer\^encias}{59}{section*.7}
