\chapter[Abordagem de Engenharia de Requisitos]{Abordagem de Engenharia de Requisitos}

\section{Abordagem Tradicional}

Por volta do ano de 1990, muitos times migraram para uma nova abordagem, esta abordagem combinava o que havia de positivo nas metodologias Cascata e Espiral, dando origem ao processo Iterativo \cite{leffingwell}, provado ser mais efetivo em uma variedade de projetos, evidenciando suas vantagens sobre as antigas metodologias.

O Processo Iterativo e Incremental consiste em 4 fases de ciclo de vida: Concepção, Elaboração, Construção, Transição. A figura abaixo exemplifica as fases do processo iterativo.

\begin{figure}[htb]
  \centering
  \label{ciclo-de-vida-rup}
    \includegraphics{figuras/ciclo-de-vida-rup.eps}
  \caption{Ciclo de vida das fases do processo iterativo (fonte: \cite{lifecycle})}
\end{figure}

Na fase de concepção, o time deve focar no entendimento do negócio, o escopo, e a viabilidade de implementação. A análise do problema é executada, e estimativas preliminares de custos são levantadas para dar suporte à viabilidade de execução do projeto \cite{leffingwell}.

A elaboração, é caracterizada pelo refinamento de requisitos previamente levantados, define-se a arquitetura do sistema e estabelece-se um protótipo, para verificar se o sistema pode ser desenvolvido pela equipe, além de servir como uma visualização prévia dos requisitos que compõe o sistema \cite{leffingwell}.

Na fase de construção, o foco é na implementação dos requisitos. Grande parte do código é produzido nesta fase, respeitando os padrões de arquitetura especificados, e também, implementados, juntamente ao design do produto que outrora foi definido \cite{leffingwell}.
Para \cite{leffingwell}, a fase de transição pode ser utilizada principalmente para a execução de testes na versão Beta de um \textit{software}, assim, os usuários e mantenedores dos sistema são treinados na própria aplicação. Após tais testes, a aplicação é migrada para a comunidade de usuários e posteriormente é realizado um deploy da aplicação.
No processo iterativo, as atividades de desenvolvimento de software são organizadas num conjunto de disciplinas. Cada disciplina consiste num arranjo lógico de atividades, e como estas atividades são ordenadas e sua ordem de execução são definidas pela metodologia.

Durante cada iteração, os times ficam o tempo que for necessário em cada disciplina. Assim, uma iteração pode ser considerada como um mini-cascata, ao longo das atividades de cada disciplina, como requisitos, análise e design, entre outras \cite{leffingwell}. A Figura 2, retirada do site do RUP (\textit{Rational Unified Process}), exemplifica as fases, disciplinas e suas durações ao longo do tempo, no processo iterativo.

\begin{figure}[htb]
  \centering
  \label{disciplinas-rup}
    \includegraphics{figuras/disciplinas-rup.eps}
  \caption{Disciplinas do processo unificado (fonte: \cite{disciplines})}
\end{figure}

\clearpage{}

\section{Abordagem Ágil}

As abordagens ágeis, mais comuns, como o Scrum, buscam melhorar a produtividade de um time de desenvolvimento, de modo que, o time busque maior empenho em suas atividades.

O \textit{Scaled Agile Framework} (SAFe) é uma metodologia Ágil que busca a integração de times, comumente chamado de times ágeis. Tal \textit{framework} possui três níveis, são eles, Time, Programa e Portfólio. O nível de Time é responsável por fornecer modelos de processos para equipes ágeis, baseadas em \textit{Scrum} e XP (\textit{Extreme Programming}). A camada de Programa integra os esforços de várias equipes ágeis para fornecer lançamentos de maior valor à empresa. O nível de Portfólio é responsável por garantir que os programas estão alinhados à estratégia de negócio e das intenções de investimento.

O SAFe é bastante utilizado quando há bastantes equipes que executam suas próprias implementações de \textit{Agile}, porém tem dificuldades quando o planejamento envolve outras equipes; busca por uma estratégia de negócio em um time Agile que poderia ser escalado em outras equipes. A imagem abaixo, também conhecida como \textit{Big Picture}, descreve o \textit{Framework}:

\begin{figure}[htb]
  \centering
  \label{niveis-safe}
    \includegraphics[keepaspectratio=true,scale=0.5]{figuras/niveis-safe.eps}
  \caption{Representação em níveis de times do SAFe (fonte: \cite{safe})}
\end{figure}

\clearpage{}

\section{Modelos de Maturidade}

\subsection{CMMI}

O CMMI, provido pelo SEI, \textit{Software Engineering Institute}, órgão integrante da universidade norte-americana \textit{Carnegie Mellon}, é um modelo de maturidade que segundo o próprio time que o desenvolveu, proporciona orientação quando utilizado no processo de desenvolvimento. Ele, por si só não é um processo ou uma descrição de processo. De forma particular, as áreas de processos que serão explicadas mais detalhadamente a seguir não mapeiam de um para um com os processos usados na organização que está sendo aplicado \cite{cmmiteam}.

Este modelo pode ser representado de duas maneiras, ou contínua ou em estágios. Esta última é particularmente mais adotada, pois define uma sequência lógica de práticas para o melhoramento, enquanto que na contínua é possível escolher de implementação das áreas de processo.

Na representação por estágios é definida uma ordem de maturidades a serem alcançadas, estabelecendo os níveis de maturidade: inicial, gerenciado, definido, quantitativamente gerenciado e em otimização, sendo que em cada nível determinadas áreas de processo vão ser mais exploradas do que outras, como visto na figura abaixo:

\begin{figure}[htb]
  \centering
  \label{niveis-cmmi}
    \includegraphics{figuras/niveis-cmmi.eps}
  \caption{Níveis do CMMI (fonte: \cite{niveiscmmi})}
\end{figure}

\clearpage{}

Por vez, as áreas de processo no CMMI consistem em um conjunto organizado de atividades, ou seja, de metas que visam aumentar a capacidade do processo a partir de determinadas práticas. As metas, juntamente com as práticas podem ser classificadas em específicas e genéricas, com a diferença básica que as específicas estão ligadas de maneira intrínseca as metas de determinada área, ao contrário das genéricas que são criadas com base nas características comuns \cite{cmmiteam}. Na figura abaixo é possível identificar essa hierarquia explanada:

\begin{figure}[htb]
  \centering
  \label{componentes-cmmi}
    \includegraphics[keepaspectratio=true,scale=0.5]{figuras/componentes-cmmi.eps}
  \caption{Componentes do modelo CMMI (fonte: \cite{chrissis2003cmmi})}
\end{figure}

\clearpage{}

\subsection{MPS.BR}

O MPS.BR é um modelo de maturidade que tem como objetivo, segundo o Guia Geral MPS de Software \cite{guiamps}, definir e aprimorar um modelo de melhoria e avaliação de processo de software e serviços, visando preferencialmente às micro, pequenas e médias empresas, de forma a atender suas necessidades de negócio e ser reconhecido nacional e internacionalmente como um modelo aplicável à indústria de software e serviços.

Este modelo é totalmente compatível com o CMMI (\textit{Capability Maturity Model Integration}), já citado acima. Sua estrutura é dividida em quatro componentes: Modelo de Referência MPS para Software, Modelo de Referência para Serviços, Método de Avaliação e Modelo de Negócio, onde cada componente é descrito por documentos ou guias do modelo MPS.BR \cite{guiamps}. Segue abaixo um esquema mostrando a organização do modelo de maturidade MPS.BR:

\begin{figure}[htb]
  \centering
  \label{componentes-mpsbr}
    \includegraphics[keepaspectratio=true,scale=0.5]{figuras/componentes-mpsbr.eps}
  \caption{Componentes do modelo MPS.BR (fonte: \cite{guiamps})}
\end{figure}

\clearpage{}

O modelo MPS.BR também define níveis de maturidade de um processo, com o objetivo estabelecer o patamar da evolução do processo de uma determinada organização \cite{guiamps}. Esses níveis caracterizam os estágios de melhoria da implementação do processo, isto é, permite que uma organização preveja seu desempenho futuro ao executar um ou mais processos \cite{guiamps}. São sete os níveis maturidade (do nível A ao nível G), onde o nível G caracteriza o nível mais inicial de maturidade de uma organização e o nível A o nível mais alto.

\begin{figure}[htb]
  \centering
  \label{niveis-maturidade-mpsbr}
    \includegraphics[keepaspectratio=true,scale=0.8]{figuras/niveis-maturidade-mpsbr.eps}
  \caption{ Níveis de maturidade do MPS.BR (fonte: \cite{imgfumsoft})}
\end{figure}

\clearpage{}