\chapter[Rastreabilidade]{Rastreabilidade}

\section{Estratégia de rastreabilidade}

Uma das tarefas principais da gerência de requisitos é possibilitar a rastreabilidade dos requisitos durante o processo de engenharia de requisitos. Se a rastreabilidade de requisitos é feita com sucesso tem-se um impacto positivo e percebe-se uma melhoria significativa no projeto sendo, então, considerada uma parte importante da engenharia de software \cite{ramesh}.

Um requisito é considerado rastreável se para qualquer parte do produto pode-se identificar o requisito que o ocasionou. Segundo Gotel e Finkelstein (1994, p. 1), a Rastreabilidade de Requisitos se refere à habilidade de escrever e seguir a vida de um requisito, tanto para ‘trás’ quanto para ‘frente’, isto é, de sua origem até sua implementação e especificação e vice-versa.

De acordo com o \textit{Rational Unified Process}, a finalidade de estabelecer a rastreabilidade de requisitos é ajudar a: (1) compreender a origem dos requisitos, (2) gerenciar o escopo do projeto, (3) gerenciar mudanças nos requisitos, (4) avaliar o impacto no projeto da mudança em um requisito, (5) avaliar o impacto da falha de um teste nos requisitos, (6) verificar se todos os requisitos do sistema são desempenhados pela implementação e (7) verificar se o sistema faz apenas o que era esperado que ele fizesse.

A rastreabilidade entre requisitos é algo importante de se realizar, pois com ela é possível verificar o fluxo do requisito, isto é, o requisito pode ser seguido desde sua origem até sua implementação \cite{hokkanen}.

Há várias classificações para se caracterizar a rastreabilidade de requisitos. As duas classificações analisadas foram a rastreabilidade vertical e horizontal de requisitos. Segundo, \cite{finkelstein}, a rastreabilidade vertical de requisitos refere-se à rastreabilidade entre fases seguintes ou anteriores na vida de um requisito no processo de desenvolvimento, enquanto que a rastreabilidade horizontal refere-se à análise entre versões e variantes de um requisito numa fase particular da vida do requisito.

\begin{figure}[htb]
  \centering
  \label{rastreabilidade}
    \includegraphics[keepaspectratio=true,scale=0.6]{figuras/rastreabilidade.eps}
  \caption{Distinção entre tipos de rastreabilidade \cite{gotel}}
\end{figure}

\clearpage{}

Como serão utilizadas várias fases para a elicitação dos requisitos (por meio de casos de uso, especificações suplementares, entre outros, como se pode ver no modelo básico na figura abaixo) e a gerência dos requisitos prevê que haverão mudanças, e essas mudanças levarão a novas versões dos requisitos, a estratégia de rastreabilidade a ser utilizada nesse projeto conterá tanto a rastreabilidade vertical quanto a rastreabilidade horizontal dos requisitos. Utilizando a rastreabilidade vertical dos requisitos será possível, a partir de um caso de uso, por exemplo, saber de qual necessidade ele se originou. O mesmo acontecerá caso se queira saber quais casos de uso originam-se a partir de determinada necessidade. Já utilizando a rastreabilidade horizontal, será possível verificar as mudanças ocorridas nos requisitos durante as atividades do processo de engenharia de requisitos proposto.

\begin{figure}[htb]
  \centering
  \label{exemplo-rastreabilidade}
    \includegraphics[keepaspectratio=true,scale=0.4]{figuras/exemplo-rastreabilidade.eps}
  \caption{Rastreabilidade Vertical RUP}
\end{figure}

\clearpage{}

\section{Atributos de requisitos}

Atributos de requisitos são as propriedades que um requisito pode ter. Estes permitem obter informações importantes sobre os requisitos e também servem para se obter a visibilidade do status do projeto de desenvolvimento. Serão utilizados três atributos para classificar os requisitos, que serão: estabilidade, prioridade e dificuldade.

\subsection{Estabilidade}

O atributo de estabilidade serve para definir a probabilidade de um requisito mudar de escopo ao longo do processo de engenharia de requisitos. Este atributo terá os seguinte valores:

\begin{table}[htb]
  \centering
  \label{my-label}

  \newcolumntype{M}{>{\begin{varwidth}{5cm}}l<{\end{varwidth}}}
  \begin{tabular}{|M|M|M}
    \cline{1-2}
    Alta & O requisito está altamente estável, isto é, tem pouca probabilidade de mudar ao longo do processo & \\ \cline{1-2}
    Média & O requisito está estável, porém há chances de o requisito mudar de escopo durante o processo & \\ \cline{1-2}
    Baixa & O requisito está instável, isto é, tem alta chance de mudar de escopo durante o processo & \\ \cline{1-2}
  \end{tabular}
  \caption{Valores assumidos para o atributo estabilidade}
\end{table}

\subsection{Prioridade}

O atributo de prioridade tem como função principal definir o nível de precedência de um requisito, isto é, a ordem de prioridade que um requisito será implementado. Este atributo poderá assumir os seguintes valores:

\begin{table}[htb]
  \centering
  \label{my-label}

  \newcolumntype{M}{>{\begin{varwidth}{5cm}}l<{\end{varwidth}}}
  \begin{tabular}{|M|M|M}
    \cline{1-2}
    Alta & Significa que o requisito tem alta importância no escopo do sistema, isto é, o sistema não consegue funcionar sem este requisito & \\ \cline{1-2}
    Média & Significa que o requisito tem uma certa importância no sistema e este consegue funcionar, mas de forma razoável & \\ \cline{1-2}
    Baixa & Significa que o requisito não é tão importante no contexto do sistema, porém o sistema consegue funcionar sem este requisito & \\ \cline{1-2}
  \end{tabular}
  \caption{Valores assumidos para o atributo prioridade}
\end{table}

\clearpage{}

\subsection{Dificuldade}

O atributo de dificuldade dos requisitos tem como finalidade definir o quão difícil será implementar o requisito proposto. Este atributo poderá assumir os seguintes valores:

\begin{table}[htb]
  \centering
  \label{my-label}

  \newcolumntype{M}{>{\begin{varwidth}{5cm}}l<{\end{varwidth}}}
  \begin{tabular}{|M|M|M}
    \cline{1-2}
    Baixa & Significa que o requisito é fácil de ser implementado & \\ \cline{1-2}
    Média & Significa que o requisito tem uma certa dificuldade para ser implementado & \\ \cline{1-2}
    Alta & Significa que o requisito é bastante difícil de ser implementado & \\ \cline{1-2}
  \end{tabular}
  \caption{Valores assumidos para o atributo dificuldade}
\end{table}

\clearpage{}